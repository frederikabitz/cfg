" ====Plugins====

call plug#begin('~/.vim/plugged')

" Repeat stuff
Plug 'tpope/vim-repeat'

" Surround stuff
Plug 'tpope/vim-surround'

" Git support
Plug 'tpope/vim-fugitive'

" Git gutter
Plug 'airblade/vim-gitgutter'

" Golang support
Plug 'fatih/vim-go'

" CtrlP
Plug 'ctrlpvim/ctrlp.vim'

call plug#end()





" ====ONLY FUNCTIONALITY AND HOTKEYS====
" LEADER KEY
let mapleader = "\<space>"

set exrc
set secure
set nu rnu
set encoding=utf-8
set tabstop=4 shiftwidth=4 expandtab
filetype plugin indent on

let g:ctrlp_match_window='bottom,order:ttb'
let g:ctrlp_switch_buffer=0
let g:ctrlp_working_path_mode=0

" Backup functionality
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

" Easily leave insert mode
imap jj <Esc>

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" disable arrow keys (for now)
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <right> <nop>
nnoremap <left> <nop>

" Highlight last inserted text
noremap gV `[v`]

" Easily swap windows
nmap <Leader>wh <Leader>ww<C-w>h<Leader>ww
nmap <Leader>wj <Leader>ww<C-w>j<Leader>ww
nmap <Leader>wk <Leader>ww<C-w>k<Leader>ww
nmap <Leader>wl <Leader>ww<C-w>l<Leader>ww

nnoremap <Leader>ov :vsp ~/.vimrc<cr>
nnoremap <Leader><Leader>o :CtrlP<cr>

" Easily enable and disable paste mode
nmap <Leader>pe :set paste<cr>:echo "Paste mode ENABLED"<cr>
nmap <Leader>pd :set nopaste<cr>:echo "Paste mode DISABLED"<cr>

" Easily switch between number and relativenumber
nnoremap <Leader>tr ToggleNumber()

" Easily clear search highlight
nmap <Leader>c :noh<cr>

" Better SCSS formatting
augroup filetype_scss
    autocmd!
    autocmd! FileType scss setlocal tabstop=2 shiftwidth=2
augroup END

" ====SETTINGS THAT INFLUENCE BOTH FUNCTIONALITY AND APPEARANCE====




" toggle between number and relativenumber
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc






" ====ONLY APPEARANCE====

syntax enable       " Enable syntax processing
set cursorline      " highlight line the cursor is currently on
set showmatch       " show matching braces
set incsearch       " search as characters are enteres
set hlsearch        "highlight matches

" A nice color theme
" set background=dark
" try
"     colorscheme solarized
" catch
" endtry

colorscheme desert


" Shade Column 120
if (exists('+colorcolumn'))
    set colorcolumn=120
    highlight ColorColumn ctermbg=DarkGray
endif


