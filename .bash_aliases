# Some ls aliases to make looking for files easier
alias ll='ls -lh'
alias la='ls -A'
alias l='ls -lAh'

# Faster than clear and more intuitive for Windows users
alias cls='clear'

# Improves navigation speed
alias cd..='cd ..'
alias ..='cd ..'

# Git
alias gitovw='printf "\n";git lg2;printf "\n\n";git status'