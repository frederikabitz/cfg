#!/bin/bash

# zz_backup file
function zz_backup {
    if [ -e "$1" ]; then
        test=0
        while [ -e "$1.BAK.$test" ]; do
            test=$((test + 1))
        done
        if [ ! "$(mv "$1" "$1.BAK.$test")" -eq 0 ]; then
            echo "Error while backing up $1"
        fi
    fi
}

# zz_replace source destination [comment char]
function zz_replace {
    if [ -e "$2" ]; then
    echo "$2 already exists, backing it up and replacing..."
    zz_backup "$2"
    cp "$1" "$2"
    if [ $# -eq 3 ]; then
        printf "%s commit $(git rev-parse HEAD)\n" "$3" >> "$2"
    fi
fi
}

# ------ BASHRC
# Call bashrc from .bashrc
if [ "$(grep --count "source $HOME/cfg/.bashrc" "$HOME/.bashrc")" -lt 1 ]; then
    echo "Adding bashrc source to .bashrc"
    echo "source $HOME/cfg/.bashrc" >> "$HOME/.bashrc"
fi

# ------ BASH_ALIASES
# Call bash_aliases from .bashrc
if [ "$(grep --count "source $HOME/cfg/.bash_aliases" "$HOME/.bashrc")" -lt 1 ]; then
    echo "Adding bash_aliases source to .bashrc"
    echo "source $HOME/cfg/.bash_aliases" >> "$HOME/.bashrc"
fi

# ------ GITCONFIG
# Replace .gitconfig
zz_replace "./.gitconfig" "$HOME/.gitconfig" "#"

# ------ TMUX.CONF
# Replace .tmux.conf
zz_replace "./.tmux.conf" "$HOME/.tmux.conf" "#"

