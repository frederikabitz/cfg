[console]::BackgroundColor = "black"
Clear-Host
[console]::BackgroundColor = "black"

Set-PSReadlineOption -BellStyle None

Import-Module posh-git

# General Utils

function l { Get-Childitem -Force $args }

# GNU Utils
If( Test-Path "$env:ProgramFiles\git\usr\bin" ){

	Set-Alias -Name lls -Value "$env:ProgramFiles\git\usr\bin\ls.exe" -Option AllScope
	function ll { & $env:ProgramFiles\git\usr\bin\ls.exe -lAh $args }
	Set-Alias -Name du -Value "$env:ProgramFiles\git\usr\bin\du.exe"

	Set-Alias -Name ssh-agent -Value "$env:ProgramFiles\git\usr\bin\ssh-agent.exe"
	Set-Alias -Name ssh-add -Value "$env:ProgramFiles\git\usr\bin\ssh-add.exe"
	Set-Alias -Name ssh-keygen -Value "$env:ProgramFiles\git\usr\bin\ssh-keygen.exe"
	Set-Alias -Name ssh -Value "$env:ProgramFiles\git\usr\bin\ssh.exe"

	Set-Alias -Name which -Value "$env:ProgramFiles\git\usr\bin\which.exe"
	Set-Alias -Name tail -Value "$env:ProgramFiles\git\usr\bin\tail.exe"
	Set-Alias -Name grep -Value "$env:ProgramFiles\git\usr\bin\grep.exe"
	Set-Alias -Name less -Value "$env:ProgramFiles\git\usr\bin\less.exe"
	Set-Alias -Name vim -Value "$env:ProgramFiles\git\usr\bin\vim.exe"
	Set-Alias -Name tar -Value "$env:ProgramFiles\git\usr\bin\tar.exe"
	Set-Alias -Name touch -Value "$env:ProgramFiles\git\usr\bin\touch.exe"

	Set-Alias -Name sha1sum -Value "$env:ProgramFiles\git\usr\bin\sha1sum.exe"
	Set-Alias -Name sha224sum -Value "$env:ProgramFiles\git\usr\bin\sha224sum.exe"
	Set-Alias -Name sha256sum -Value "$env:ProgramFiles\git\usr\bin\sha256sum.exe"
	Set-Alias -Name sha384sum -Value "$env:ProgramFiles\git\usr\bin\sha384sum.exe"
	Set-Alias -Name sha512sum -Value "$env:ProgramFiles\git\usr\bin\sha512sum.exe"
	
}

If( Test-Path "$env:ProgramFiles\git\usr\bin\tree.exe"){
	Set-Alias -Name tree -Value "$env:ProgramFiles\git\usr\bin\tree.exe"
}

Start-SshAgent -Quiet

# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}
